using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

[Route("api/[controller]")]
[Authorize]
public class AccountingController : Controller
{
    private readonly AccountingOfWorkingHoursContext _context;
    private readonly ITokenService _tokenService;

    public AccountingController(AccountingOfWorkingHoursContext context, ITokenService tokenService)
    {
        this._context = context;
        this._tokenService = tokenService;
    }

    [HttpGet]
    public async Task<IActionResult> GetAccounting([FromQuery] int year, [FromQuery] int month, [FromQuery] int stockId)
    {
        List<AccountingEmployee> employees = GetEmployeesOfCurrentStock(stockId, month, year);

        List<Accounting> res = new();

        foreach (var employee in employees)
        {
            Accounting? accounting = await _context.Accountings.FindAsync(employee.EmployeeId, month, year);

            WorkPlan? currentEmployeeWorkPlan = _context.WorkPlans.Where(w => w.EmployeeId == employee.EmployeeId
            && w.Year == year
            && w.Month == month).FirstOrDefault(); // TODO: Rewrite better

            if (currentEmployeeWorkPlan is null)
                continue;

            Accounting accountingForCurrentEmployee = new();
            accountingForCurrentEmployee.EmployeeId = employee.EmployeeId;
            accountingForCurrentEmployee.Year = year;
            accountingForCurrentEmployee.Month = month;
            accountingForCurrentEmployee.WorkPlanId = currentEmployeeWorkPlan.WorkPlanId;

            if (accounting is null)
            {
                accountingForCurrentEmployee.Advance = employee.Salary * 0.5m; // Аванс
                await _context.Accountings.AddAsync(accountingForCurrentEmployee);
            }
            else
                accountingForCurrentEmployee = accounting;

            ShiftSummary shiftsummary = GetCountOfDayAndNightHours(employee.EmployeeId, month, year);
            int hoursWorkedDay = shiftsummary.CountOfDayHours;
            int hoursOvertimeDay = Math.Max(0, hoursWorkedDay - (currentEmployeeWorkPlan.NumberOfDayShifts * currentEmployeeWorkPlan.NumberOfHoursPerDayShift));
            int hoursWorkedNight = shiftsummary.CountOfNightHours;
            int hoursOvertimeNight = Math.Max(0, hoursWorkedNight - (currentEmployeeWorkPlan.NumberOfNightShifts * currentEmployeeWorkPlan.NumberOfHoursPerNightShift));

            accountingForCurrentEmployee.OvertimeDay = hoursOvertimeDay;
            accountingForCurrentEmployee.OvertimeNight = hoursOvertimeNight;

            decimal salaryForShift = employee.Salary / currentEmployeeWorkPlan.NumberOfDayShifts;
            decimal salaryForHour = salaryForShift / currentEmployeeWorkPlan.NumberOfHoursPerDayShift;

            accountingForCurrentEmployee.SalaryForShift = salaryForShift;
            accountingForCurrentEmployee.SalaryForHour = salaryForHour;

            accountingForCurrentEmployee.Seniority = ((year * 12 + month - (employee.Seniority.Year * 12 + employee.Seniority.Month)) / 6) * 500;

            decimal salaryForOvertime = salaryForHour * 1.5m;
            accountingForCurrentEmployee.Earned = (hoursWorkedDay + hoursWorkedNight) * salaryForHour + (hoursOvertimeDay + hoursOvertimeNight) * salaryForOvertime + accountingForCurrentEmployee.Bonus + accountingForCurrentEmployee.Mentoring + accountingForCurrentEmployee.Teaching + accountingForCurrentEmployee.Seniority + accountingForCurrentEmployee.Vacation;

            accountingForCurrentEmployee.Penalties = shiftsummary.Penalties;
            accountingForCurrentEmployee.Sends = shiftsummary.Sends;

            accountingForCurrentEmployee.Payment = accountingForCurrentEmployee.Earned - (accountingForCurrentEmployee.Advance + accountingForCurrentEmployee.Penalties + accountingForCurrentEmployee.Sends);

            res.Add(accountingForCurrentEmployee);
        }

        await _context.SaveChangesAsync();

        return Ok(res);
    }

    private List<AccountingEmployee> GetEmployeesOfCurrentStock(int stockId, int month, int year)
    {
        IEnumerable<AccountingEmployee> result = _context.ShiftInfos.Include(s => s.ShiftHistory)
            .Where(s => s.DateAndTimeOfArrival!.Month == month)
            .Where(s => s.DateAndTimeOfArrival!.Year == year)
            .Include(s => s.Employee)
                .ThenInclude(e => e!.Position)
                    .Where(s => s.Employee!.Stocks == $"[{stockId}]")
                    .Select(shiftInfo => new AccountingEmployee
                    {
                        EmployeeId = shiftInfo.EmployeeId,
                        FullName = $"{shiftInfo.Employee!.Surname} {shiftInfo.Employee.Name} {shiftInfo.Employee.Patronymic}",
                        PositionName = shiftInfo.Employee.Position!.Name,
                        Salary = shiftInfo.Employee.Salary,
                        Seniority = shiftInfo.Employee.StartOfLuchSeniority
                    }).Distinct();

        return result.ToList();
    }

    private ShiftSummary GetCountOfDayAndNightHours(Guid employeeId, int month, int year)
    {
        List<ShiftInfo> result = _context.ShiftInfos
            .Where(s => s.DateAndTimeOfArrival!.Month == month
                && s.DateAndTimeOfArrival!.Year == year
                && s.EmployeeId == employeeId).ToList();

        int countOfDayHours = 0, countOfNightHours = 0;
        decimal penalties = 0, sends = 0;
        foreach (var record in result)
        {
            if (record.DayOrNight == "День")
                countOfDayHours += record.NumberOfHoursWorked;
            else
                countOfNightHours += record.NumberOfHoursWorked;

            if (record.Penalty is not null)
                penalties += (decimal)record.Penalty;

            if (record.Send is not null)
                sends += (decimal)record.Send;
        }

        return new ShiftSummary
        {
            CountOfDayHours = countOfDayHours,
            CountOfNightHours = countOfNightHours,
            Penalties = penalties,
            Sends = sends
        };
    }

    [HttpPatch]
    public async Task<IActionResult> UpdateAccountings([FromBody] List<AccountingUpdateDTO> records)
    {
        Employee? currentEmploye = _tokenService.GetCurrentEmployee(User, _context);

        if (currentEmploye is null)
            return NotFound("Сотрудник не найден");

        await _context.Entry(currentEmploye).Reference(e => e.Position).LoadAsync();

        if (currentEmploye.Position!.Name != "Начальник склада" && currentEmploye.Position!.Name != "Администратор")
            return Forbid("У вас недостаточно прав для редактирования");

        foreach (var record in records)
        {
            Accounting? accounting = await _context.Accountings.FindAsync(record.EmployeeId, record.Month, record.Year);

            if (accounting is null)
                return NotFound($"Запись с учетом для сотрудника {record.EmployeeId} не найдена");

            AccountingHistory accountingHistory = new();

            accountingHistory.EmployeeId = record.EmployeeId;
            accountingHistory.Month = record.Month;
            accountingHistory.Year = record.Year;

            // Writing previous values into history
            accountingHistory.MentoringPrev = accounting.Mentoring;
            accountingHistory.TeachingPrev = accounting.Teaching;
            accountingHistory.BonusPrev = accounting.Bonus;
            accountingHistory.VacationPrev = accounting.Vacation;
            accountingHistory.AdvancePrev = accounting.Advance;

            // Writing new values into main table
            accounting.Mentoring = record.Mentoring;
            accounting.Teaching = record.Teaching;
            accounting.Bonus = record.Bonus;
            accounting.Vacation = record.Vacation;
            accounting.Advance = record.Advance;

            // Writing new values into history
            accountingHistory.Mentoring = record.Mentoring;
            accountingHistory.Teaching = record.Teaching;
            accountingHistory.Bonus = record.Bonus;
            accountingHistory.Vacation = record.Vacation;
            accountingHistory.Advance = record.Advance;

            accountingHistory.UpdateDate = DateTime.UtcNow;

            await _context.AccountingHistories.AddAsync(accountingHistory);
        }

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (Exception ex)
        {
            return StatusCode(500, ex);
        }

        return Ok();
    }
}