using API.Models;
using Newtonsoft.Json;

public class AccountingHistory : AccountingUpdateDTO
{
    public Guid AccountingHistoryId { get; set; }

    public decimal MentoringPrev { get; set; }

    public decimal TeachingPrev { get; set; }

    public decimal BonusPrev { get; set; }

    public decimal VacationPrev { get; set; }

    public decimal AdvancePrev { get; set; }

    public DateTime UpdateDate { get; set; }

    [JsonIgnore]
    public virtual Employee Employee { get; set; } = null!;
}