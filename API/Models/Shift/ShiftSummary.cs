public class ShiftSummary
{
    public int CountOfDayHours { get; set; }

    public int CountOfNightHours { get; set; }

    public decimal Penalties { get; set; }

    public decimal Sends { get; set; }
}